<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends AbstractController
{
    public function indexAction(): Response
    {
        $user = $this->getUser();

        if (!$user) {
            return $this->redirectToRoute('login');
        }

        return match($user->getRoles()[0]) {
            'ROLE_DEV',
            'ROLE_PERFORMANCE',
            'ROLE_CRON',
            'ROLE_GRANT',
            'ROLE_ADMIN' => $this->redirect('http://localhost:8080'),
            default => $this->render('index.html.twig'),
        };

    }
}
