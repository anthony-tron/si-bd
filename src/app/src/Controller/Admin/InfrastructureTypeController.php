<?php

namespace App\Controller\Admin;

use App\Entity\InfrastructureType;
use App\Form\InfrastructureTypeType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InfrastructureTypeController extends CRUDController
{
    public function __construct()
    {
        parent::__construct(InfrastructureType::class, InfrastructureTypeType::class, 'si_admin', null , ['genre' => 'un', 'label' => 'Type d\'infrastructure']);
    }
    protected function getRedirection(): RedirectResponse
    {
        return $this->redirectToRoute('admin_list_entity', [
            'entityClass' => 'EquestrianCenter',
        ]);
    }

}
