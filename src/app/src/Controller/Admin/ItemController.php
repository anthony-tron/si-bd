<?php

namespace App\Controller\Admin;

use App\Controller\Admin\CRUDController;
use App\Entity\Item;
use App\Form\ItemType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ItemController extends CRUDController
{
    public function __construct()
    {
        parent::__construct(Item::class, ItemType::class, 'si_admin', null, ['genre' => 'un', 'label' => 'item']);
    }
    protected function getRedirection(): RedirectResponse
    {
        return $this->redirectToRoute('admin_list_entity', [
            'entityClass' => 'EquestrianCenter',
        ]);
    }

}
