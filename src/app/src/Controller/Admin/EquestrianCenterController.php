<?php

namespace App\Controller\Admin;

use App\Entity\EquestrianCenter;
use App\Form\EquestrianCenterType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EquestrianCenterController extends CRUDController
{
    public function __construct()
    {
        parent::__construct(EquestrianCenter::class, EquestrianCenterType::class, 'si_admin', null, ['genre' => 'un', 'label' => 'Centre équestre']);
    }

    protected function getRedirection(): RedirectResponse
    {
        return $this->redirectToRoute('admin_list_entity', [
            'entityClass' => 'EquestrianCenter',
        ]);
    }

}
