<?php

namespace App\Controller\Admin;

use App\Entity\Horse\Horse;
use App\Form\HorseType;
use App\Service\ListBuilder;
use App\Normalizer\HorseNormalizer;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HorseController extends CRUDController
{
    public function __construct()
    {
        parent::__construct(Horse::class, HorseType::class, 'horse_specialist', null, ['genre' => 'un', 'label' => 'cheval']);
    }
    public function list(
        Request $req,
        ManagerRegistry $doctrine,
        HorseNormalizer $normalizer,
        ListBuilder $listBuilder
    ): Response
    {
        $objects = $listBuilder
            ->setRepository($doctrine->getManager()->getRepository(Horse::class))
            ->setPage($req->query->get('page', 0))
            ->setMaxResults(10)
            ->setNormalizer($normalizer)
            ->getObjects();

        return $this->render('admin/list.html.twig', [
            'headers' => array_keys($objects[0]),
            'objects' => $objects,
        ]);
    }

    public function preSetObject(Request $request, $object)
    {
        return $object;
    }

    protected function getRedirection(): RedirectResponse
    {
        return $this->redirectToRoute('admin_list_entity', [
            'entityClass' => 'EquestrianCenter',
        ]);
    }

}