<?php

namespace App\Controller\Admin;

use App\Controller\Admin\CRUDController;
use App\Entity\ItemFamily;
use App\Form\ItemFamilyType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ItemFamilyController extends CRUDController
{
    public function __construct()
    {
        parent::__construct(ItemFamily::class, ItemFamilyType::class, 'si_admin', null, ['genre' => 'une', 'label' => 'famille d\'item']);
    }
    protected function getRedirection(): RedirectResponse
    {
        return $this->redirectToRoute('admin_list_entity', [
            'entityClass' => 'EquestrianCenter',
        ]);
    }

}
