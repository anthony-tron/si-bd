<?php

namespace App\Controller\Admin;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class CRUDController extends AbstractController
{
    public function __construct(protected $entityClass, protected $formClass, protected $userManager, protected ?string $formTemplate = null, protected array $options = [])
    {
    }

    /**
     * @param Request $request
     * @param ManagerRegistry $doctrine
     * @return Response
     */
    public function create(Request $request, ManagerRegistry $doctrine) : Response
    {
        $object = new $this->entityClass();
        $object = $this->preSetObject($request, $object);
        $form = $this->createForm($this->formClass, $object);

        if (!$response = $this->persistForm($request, $form, $doctrine)) {
            return $this->render($this->formTemplate ?? 'admin/form.html.twig', array_merge(
                ['form' => $form->createView(), 'action' => 'Nouveau'],
                $this->options
            ));
        }
        return $response;
    }

    /**
     * @param Request $request
     * @param $id
     * @param ManagerRegistry $doctrine
     * @return Response
     */
    public function edit(Request $request, $id, ManagerRegistry $doctrine) : Response
    {
        $repository = $doctrine->getManager()->getRepository($this->entityClass);
        $object = $repository->find($id);
        dd($object);
        $form = $this->createForm($this->formClass, $object);

        if (!$response = $this->persistForm($request, $form, $doctrine)) {
            return $this->render($this->formTemplate ?? 'admin/form.html.twig', array_merge(
                ['form' => $form->createView(), 'action' => 'Editer'],
                $this->options
            ));
        }
        return $response;
    }

    protected function persistForm(Request $request, FormInterface $form, ManagerRegistry $doctrine)
    {
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $object = $form->getData();
            $object = $this->postSetObject($request, $object);
            $em = $doctrine->getManager($this->userManager);

            $em->persist($object);
            $em->flush();

            return $this->getRedirection();
        }
        return false;
    }

    /**
     * @param Request $request
     * @param $object
     * @return mixed
     */
    public function preSetObject(Request $request, $object)
    {
        return $object;
    }

    /**
     * @param Request $request
     * @param $object
     * @return mixed
     */
    public function postSetObject(Request $request, $object)
    {
        return $object;
    }

    abstract protected function getRedirection(): RedirectResponse;
}
