<?php

namespace App\Controller\Admin;

use App\Entity\RidingClub;
use App\Form\RidingClubType;
use Symfony\Component\HttpFoundation\RedirectResponse;

class RidingClubController extends CRUDController
{
    public function __construct()
    {
        parent::__construct(RidingClub::class, RidingClubType::class, 'si_admin');
    }
    protected function getRedirection(): RedirectResponse
    {
        return $this->redirectToRoute('admin_list_entity', [
            'entityClass' => 'EquestrianCenter',
        ]);
    }

}
