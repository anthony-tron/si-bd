<?php

namespace App\Controller\Admin;

use App\Entity\Horse\Breed;
use App\Form\BreedType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BreedController extends CRUDController
{
    public function __construct()
    {
        parent::__construct(Breed::class, BreedType::class, 'horse_specialist', null, ['genre' => 'une', 'label' => 'race']);
    }

    protected function getRedirection(): RedirectResponse
    {
        return $this->redirectToRoute('admin_list_entity', [
            'entityClass' => 'Breed',
        ]);
    }

}
