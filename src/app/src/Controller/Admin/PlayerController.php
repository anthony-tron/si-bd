<?php

namespace App\Controller\Admin;

use App\Form\PlayerType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class PlayerController extends CRUDController
{
    public function __construct(
        private UserPasswordHasherInterface $passwordHasher,
    )
    {
        parent::__construct('App\Entity\Player\Player', PlayerType::class, 'comunity_manager', null, ['genre' => 'un', 'label' => 'joueur']);
    }

    public function preSetObject(Request $request, $object)
    {
        $object->setLastlyConnectedAt(null);
        $object->setIp($request->server->get('REMOTE_ADDR'));

        return $object;
    }

    public function postSetObject(Request $request, $object) {
        $object->setPassword(
            $this->passwordHasher->hashPassword(
                $object,
                $object->getPassword(),
            )
        );

        return $object;
    }

    protected function getRedirection(): RedirectResponse
    {
        return $this->redirectToRoute('admin_list_entity', [
            'entityClass' => 'Player',
        ]);
    }

}