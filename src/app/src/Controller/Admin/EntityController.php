<?php

namespace App\Controller\Admin;

use App\Service\EntityRegistry;
use App\Service\NormalizerRegistry;
use App\Service\ListBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EntityController extends AbstractController
{

    public function delete(
        ManagerRegistry $managerRegistry,
        Request $request,
        LoggerInterface $logger,
        EntityRegistry $entityRegistry,
    )
    {
        $fullEntityClass = $entityRegistry->resolveEntityClass($request->get('entity_class'));

        try
        {
            $manager = $managerRegistry->getManager();

            $repository = $manager->getRepository($fullEntityClass);
            $entity = $repository->find($request->get('entity_id'));

            $manager->remove($entity);
            $manager->flush();

            $this->addFlash('success', 'Removal of entity successful.');
        }
        catch (\Exception $e)
        {
            $this->addFlash('error', 'Could not remove this entity.');

            $logger->error('Error while trying to delete an entity.', [
                'details' => $e,
            ]);
        }

        return $this->redirect($request->get('redirect_to'));
    }

    public function list(
        Request $req,
        ManagerRegistry $doctrine,
        NormalizerRegistry $nr,
        ListBuilder $listBuilder,
        EntityRegistry $entityRegistry,
        string $entityClass,
    ): Response
    {
        $fullEntityClass = $entityRegistry->resolveEntityClass($entityClass);

        $objects = $listBuilder
            ->setRepository($doctrine->getManager()->getRepository($fullEntityClass))
            ->setPage($req->query->get('page', 0))
            ->setMaxResults(10)
            ->setNormalizer($nr->getNormalizer($fullEntityClass))
            ->getObjects();

        return $this->render('admin/list.html.twig', [
            'entity_class' => $entityClass,
            'redirect_to' => $req->getUri(),
            'headers' => count($objects) > 0 ? array_keys($objects[0]) : null,
            'objects' => $objects,
            'page' => $req->query->get('page') ?? 0,
        ]);
    }


}