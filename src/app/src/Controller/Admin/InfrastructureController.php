<?php

namespace App\Controller\Admin;

use App\Controller\Admin\CRUDController;
use App\Entity\Infrastructure;
use App\Form\InfrastructureType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InfrastructureController extends CRUDController
{
    public function __construct()
    {
        parent::__construct(Infrastructure::class, InfrastructureType::class, 'si_admin', null, ['genre' => 'une', 'label' => 'infrastructure']);
    }
    protected function getRedirection(): RedirectResponse
    {
        return $this->redirectToRoute('admin_list_entity', [
            'entityClass' => 'EquestrianCenter',
        ]);
    }

}
