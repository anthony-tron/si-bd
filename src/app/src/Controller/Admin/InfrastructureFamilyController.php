<?php

namespace App\Controller\Admin;

use App\Entity\InfrastructureFamily;
use App\Form\InfrastructureFamilyType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InfrastructureFamilyController extends CRUDController
{
    public function __construct()
    {
        parent::__construct(InfrastructureFamily::class, InfrastructureFamilyType::class, 'si_amdin', null, ['genre' => 'une', 'label' => 'famille d\'infrastructure']);
    }
    protected function getRedirection(): RedirectResponse
    {
        return $this->redirectToRoute('admin_list_entity', [
            'entityClass' => 'EquestrianCenter',
        ]);
    }

}
