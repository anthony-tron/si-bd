<?php

namespace App\Controller\Admin;

use App\Controller\Admin\CRUDController;
use App\Entity\ItemType;
use App\Form\ItemTypeType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ItemTypeController extends CRUDController
{
    public function __construct()
    {
        parent::__construct(ItemType::class, ItemTypeType::class, 'si_admin', null, ['genre' => 'un', 'label' => 'type d\'item']);
    }
    protected function getRedirection(): RedirectResponse
    {
        return $this->redirectToRoute('admin_list_entity', [
            'entityClass' => 'EquestrianCenter',
        ]);
    }

}
