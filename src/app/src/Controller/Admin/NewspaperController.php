<?php

namespace App\Controller\Admin;

use App\Controller\Admin\CRUDController;
use App\Entity\Newspaper\Newspaper;
use App\Form\NewsPaperType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NewspaperController extends CRUDController
{
    public function __construct()
    {
        parent::__construct(Newspaper::class, NewsPaperType::class, 'publisher', 'admin/form/newspaper_form.html.twig');
    }

    public function edit(Request $request, $id, ManagerRegistry $doctrine) : Response
    {
        $em = $doctrine->getManager();
        if (null === $newspaper = $em->getRepository(Newspaper::class)->find($id)) {
            throw $this->createNotFoundException('No newspaper found for id '.$id);
        }

        $originalArticles = new ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($newspaper->getArticles() as $article) {
            $originalArticles->add($article);
        }

        $form = $this->createForm(NewsPaperType::class, $newspaper);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($originalArticles as $article) {
                if (false === $newspaper->getArticles()->contains($article)) {
                    $article->setNewspaper(null);

                    $em->remove($article);
                }
            }
            $em->persist($newspaper);
            $em->flush();

            // redirect back to some edit page
            return $this->redirectToRoute('admin_newspaper_edit', ['id' => $id]);
        }

        return $this->render($this->formTemplate ?? 'admin/form.html.twig', ['form' => $form->createView()]);
    }

    public function postSetObject(Request $request, $object)
    {
        return $object->setDate(new \DateTime());
    }

    protected function getRedirection(): RedirectResponse
    {
        return $this->redirectToRoute('admin_list_entity', [
            'entityClass' => 'Newspaper',
        ]);
    }

}
