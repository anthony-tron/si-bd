<?php

namespace App\Controller\Admin;

use App\Entity\Horse\Disease;
use App\Form\DiseaseType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DiseaseController extends CRUDController
{
    public function __construct()
    {
        parent::__construct(Disease::class, DiseaseType::class, 'horse_specialist', null, ['genre' => 'une', 'label' => 'maladie']);
    }

    protected function getRedirection(): RedirectResponse
    {
        return $this->redirectToRoute('admin_list_entity', [
            'entityClass' => 'Disease',
        ]);
    }

}