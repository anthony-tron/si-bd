<?php

namespace App\Controller\Admin;

use App\Service\NormalizerRegistry;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{

    public function indexAction(Request $request) {
        return $this->render('admin/index.html.twig', [
            'entities' => array_map(
                fn($e) => [
                    'short_name' => (new \ReflectionClass($e))->getShortName(),
                    'list_href' => '/admin/list/' . (new \ReflectionClass($e))->getShortName(),
                    'create_href' => '/admin/' . strtolower((new \ReflectionClass($e))->getShortName()) . '/new',
                ],
                array_keys(NormalizerRegistry::map),
            ),
        ]);

    }

}