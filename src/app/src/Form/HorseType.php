<?php

namespace App\Form;

use App\Entity\Cron;
use App\Entity\Horse\Breed;
use App\Entity\Horse\Disease;
use App\Entity\Horse\Horse;
use App\Entity\Horse\Parasite;
use App\Entity\Horse\Wound;
use App\Entity\Item;
use App\Entity\Player\Player;
use phpDocumentor\Reflection\Types\String_;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HorseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, ['help' => 'Nom du cheval (ex: Epona, Spirit)', 'invalid_message'=>'doit contenir une chaine de caractères', 'attr'=> ['class' => 'form-control']])
            ->add('breed',EntityType::class, ['class' => Breed::class, 'help' => 'choix de la race du cheval', 'invalid_message' => 'doit être choisi', 'attr'=> ['class' => 'form-control']])
            ->add('resistance', NumberType::class,['required' => false, 'help'=>'valeur de résistance (0-100)','invalid_message'=>'doit contenir des nombres', 'attr' => ['class' => 'form-control']])
            ->add('stamina', NumberType::class,['required' => false,'help'=>'valeur de stamina (0-100)','invalid_message'=>'doit contenir des nombres', 'attr'=> ['class' => 'form-control']])
            ->add('spring', NumberType::class,['required' => false,'help'=>'valeur de détente (0-100)','invalid_message'=>'doit contenir des nombres', 'attr'=> ['class' => 'form-control']])
            ->add('speed', NumberType::class,['required' => false,'help'=>'valeur de vitesse (0-100)','invalid_message'=>'doit contenir des nombres', 'attr'=> ['class' => 'form-control']])
            ->add('sociability', NumberType::class,['required' => false,'help'=>'valeur de sociabilité (0-100)','invalid_message'=>'doit contenir des nombres', 'attr'=> ['class' => 'form-control']])
            ->add('intelligence', NumberType::class,['required' => false,'help' =>"valeur d'intelligence (0-100)",'invalid_message'=>'doit contenir des nombres', 'attr'=> ['class' => 'form-control']])
            ->add('temperament', NumberType::class,['required' => false,'help'=>'temperament (0-100)','invalid_message'=>'doit contenir des nombres', 'attr'=> ['class' => 'form-control']])
            ->add('health', NumberType::class,['required' => false,'help'=>'valeur de vie (0-100)','invalid_message'=>'doit contenir des nombres', 'attr'=> ['class' => 'form-control']])
            ->add('morale', NumberType::class,['required' => false,'help'=>'valeur de morale (0-100)','invalid_message'=>'doit contenir des nombres', 'attr'=> ['class' => 'form-control']])
            ->add('stress', NumberType::class,['required' => false,'help'=>'valeur de stresse (0-100)','invalid_message'=>'doit contenir des nombres', 'attr'=> ['class' => 'form-control']])
            ->add('tiredness', NumberType::class,['required' => false,'help'=>'valeur de fatigue (0-100)','invalid_message'=>'doit contenir des nombres', 'attr'=> ['class' => 'form-control']])
            ->add('hunger', NumberType::class, ['required' => false, 'help'=>'valeur de faim (0-100)','invalid_message'=>'doit contenir des nombres', 'attr'=> ['class' => 'form-control']])
            ->add('cleanliness', NumberType::class, ['required' => false,'help'=>'valeur de propreté (0-100)','invalid_message'=>'doit contenir des nombres', 'attr'=> ['class' => 'form-control']])
            ->add('player',EntityType::class, ['class' =>Player::class,"required" => false, 'help' => 'choix du joueur associé', 'invalid_message' => 'doit être choisi', 'attr'=> ['class' => 'form-select'] ])
            ->add('item', EntityType::class, ['class' => Item::class, 'multiple' => true,'required' => false, 'attr' => ['class' => 'form-select']])
            ->add('disease', EntityType::class, ['class' => Disease::class, 'multiple' => true,'required' => false, 'attr' => ['class' => 'form-select']])
            ->add('parasit', EntityType::class, ['class' => Parasite::class, 'multiple' => true,'required' => false, 'attr' => ['class' => 'form-select']])
            ->add('wound', EntityType::class, ['class' => Wound::class, 'multiple' => true,'required' => false, 'attr' => ['class' => 'form-select']])
            ->add('submit',SubmitType::class, ['attr' => ['class' => 'btn btn-primary']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Horse::class,
        ]);
    }
}
