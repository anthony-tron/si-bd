<?php

namespace App\Form;

use App\Entity\Player\Player;
use PhpParser\Node\Scalar\MagicConst\File;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Url;

class PlayerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username', TextType::class, ['required' => true, 'label' => 'Nom d\'utilisateur', 'attr' => ['class' => 'form-control m-3']])
            ->add('email', EmailType::class, ['required' => true, 'label' => 'Email', 'attr' => ['class' => 'form-control m-3']])
            ->add('firstName', TextType::class, ['required' => true, 'label' => 'Prénom', 'attr' => ['class' => 'form-control m-3']])
            ->add('lastName', TextType::class, ['required' => true, 'label' => 'Nom', 'attr' => ['class' => 'form-control m-3']])
            ->add('gender', ChoiceType::class, [
                'required' => true,
                'label' => 'genre',
                'choices' => [
                    'MALE' => 'MALE',
                    'FEMALE' => 'FEMALE',
                    'NON BINARY' => 'NON_BINARY',
                ],
                'attr' => ['class' => 'form-select m-3']
            ])
            ->add('role', ChoiceType::class, [
                'required' => true,
                'label' => 'role',
                'choices' => [
                    'ROLE_USER' => 'ROLE_USER',
                    'ROLE_ADMIN' => 'ROLE_ADMIN',
                    'ROLE_PUBLISHER' => 'ROLE_PUBLISHER',
                    'ROLE_CLIENT' => 'ROLE_CLIENT',
                    'ROLE_HORSE_SPECIALIST' => 'ROLE_HORSE_SPECIALIST',
                    'ROLE_CONTEST_SPECIALIST' => 'ROLE_CONTEST_SPECIALIST',
                    'ROLE_DEV' => 'ROLE_DEV',
                    'ROLE_PERFORMANCE' => 'ROLE_PERFORMANCE',
                    'ROLE_CRON' => 'ROLE_CRON',
                    'ROLE_GRANT' => 'ROLE_GRANT'
                ],
                'attr' => ['class' => 'form-select m-3']
            ])
            ->add('birthday', DateType::class, ['widget' => 'single_text','required' => true, 'label' => 'Date de naissance', 'attr' => ['class' => 'form-select m-3']])
            ->add('phone', TelType::class, ['label' => 'Tel', 'required' => false,'invalid_message'=>'doit être au format 0x.xx.xx.xx.xx', 'attr' => ['class' => 'form-control m-3']])
            ->add('address', TextType::class, ['label' => 'Adresse postale', 'required' => false,'invalid_message'=>'doit contenir une chaine de caractères', 'attr' => ['class' => 'form-control m-3']])
            ->add('description', TextareaType::class,['invalid_message'=>'doit contenir une chainede caractères', 'attr' => ['class' => 'form-control m-3']])
            ->add('webSite', UrlType::class, ['label' => 'Site web', 'required' => false,'invalid_message'=>'doit contenir une url', 'attr' => ['class' => 'form-control m-3']])
            ->add('media', FileType::class, ['label' => 'image', 'required' => false,'invalid_message'=>'doit contenir un fichier', 'attr' => ['class' => 'form-control m-3']])
            ->add('password', PasswordType::class, ['label' => 'Mot de passe', 'attr' => ['class' => 'form-control m-3']])
            ->add('submit', SubmitType::class, ['label' => 'Créer un joueur', 'attr' => ['class' => 'text-center btn btn-primary m-3']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Player::class,
        ]);
    }
}
