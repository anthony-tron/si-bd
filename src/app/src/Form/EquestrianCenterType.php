<?php

namespace App\Form;

use App\Entity\Cron;
use App\Entity\EquestrianCenter;
use App\Entity\Infrastructure;
use App\Entity\Player\Player;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EquestrianCenterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('capacity', NumberType::class, ['required' => true, 'attr' => ['class' => 'form-control']])
            ->add('owner', EntityType::class, ['class' => Player::class, 'required' => false, 'attr' => ['class' => 'form-select']])
            ->add('cron', EntityType::class, ['class' => Cron::class, 'multiple' => true,'required' => false, 'attr' => ['class' => 'form-select']])
            ->add('infrastructure', EntityType::class, ['class' => Infrastructure::class, 'multiple' => true,'required' => false, 'attr' => ['class' => 'form-select']])
        ;
    }
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EquestrianCenter::class,
        ]);
    }
}
