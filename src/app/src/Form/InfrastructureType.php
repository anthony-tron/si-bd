<?php

namespace App\Form;

use App\Entity\Horse\Horse;
use App\Entity\Infrastructure;
use App\Entity\InfrastructureFamily;
use App\Entity\Item;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InfrastructureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('resources', NumberType::class, ['required' => true, 'attr' => ['class' => 'form-control']])
            ->add('description', TextareaType::class, ['required' => true, 'attr' => ['class' => 'form-control']])
            ->add('itemCapacity', NumberType::class, ['required' => true, 'attr' => ['class' => 'form-control']])
            ->add('horseCapacity', NumberType::class, ['required' => true, 'attr' => ['class' => 'form-control']])
            ->add('price', NumberType::class, ['required' => true, 'attr' => ['class' => 'form-control']])
            ->add('level', NumberType::class, ['required' => true, 'attr' => ['class' => 'form-control']])
            ->add('family', EntityType::class, ['class' => InfrastructureFamily::class, 'attr' => ['class' => 'form-select']])
            ->add('type', EntityType::class, ['class' => \App\Entity\InfrastructureType::class, 'attr' => ['class' => 'form-select']])
            ->add('horse', EntityType::class, ['class' => Horse::class, 'multiple' => true, 'attr' => ['class' => 'form-select']])
            ->add('item', EntityType::class, ['class' => Item::class, 'multiple' => true, 'attr' => ['class' => 'form-select']])
            ->add('submit',SubmitType::class, ['attr' => ['class' => 'btn btn-primary']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Infrastructure::class,
        ]);
    }
}
