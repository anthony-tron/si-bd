<?php

namespace App\Form;

use App\Entity\Infrastructure;
use App\Entity\Player\Player;
use App\Entity\RidingClub;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RidingClubType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('capacity', NumberType::class, ['required' => true, 'attr' => ['class' => 'form-control']])
            ->add('membershipPrice', NumberType::class, ['required' => true, 'attr' => ['class' => 'form-control']])
            ->add('owner', EntityType::class, ['class' => Player::class, 'required' => true, 'attr' =>  ['class' => 'form-control']])
            ->add('player', EntityType::class, ['class' => Player::class, 'multiple' => true, 'required' => true, 'attr' =>  ['class' => 'form-control']])
            ->add('infrastructure', EntityType::class, ['class' => Infrastructure::class, 'multiple' => true,'required' => true, 'attr' =>  ['class' => 'form-control']])
            ->add('submit', SubmitType::class, ['label' => 'Créer un joueur', 'attr' => ['class' => 'text-center btn btn-primary m-3']])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RidingClub::class,
        ]);
    }
}
