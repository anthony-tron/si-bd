<?php

namespace App\Form;

use App\Entity\Item;
use App\Entity\ItemFamily;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, ['required' => true, 'attr' => ['class' => 'form-control']])
            ->add('level', NumberType::class, ['required' => true, 'attr' => ['class' => 'form-control']])
            ->add('value', NumberType::class, ['required' => true, 'attr' => ['class' => 'form-control']])
            ->add('family', EntityType::class, ['class' => ItemFamily::class, 'attr' => ['class' => 'form-control']] )
            ->add('type', EntityType::class, ['class' => \App\Entity\ItemType::class, 'attr' => ['class' => 'form-control']])
            ->add('submit',SubmitType::class, ['attr' => ['class' => 'btn btn-primary']])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Item::class,
        ]);
    }
}
