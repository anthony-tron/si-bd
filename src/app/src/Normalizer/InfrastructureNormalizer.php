<?php

namespace App\Normalizer;

use App\Entity\Infrastructure;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class InfrastructureNormalizer implements NormalizerInterface
{

    /**
     * @inheritDoc
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof Infrastructure;
    }

    /**
     * @inheritDoc
     */
    public function normalize(mixed $object, string $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'type' => $object->getType()->getName(),
            'family' => $object->getFamily()->getName(),
            'resources' => $object->getResources(),
            'description' => $object->getDescription(),
            'item_capacity' => $object->getItemCapacity(),
            'horse_capacity' => $object->getHorseCapacity(),
            'price' => $object->getPrice(),
            'level' => $object->getLevel(),
        ];
    }
}