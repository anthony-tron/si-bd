<?php

namespace App\Normalizer;

use App\Entity\InfrastructureFamily;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class InfrastructureFamilyNormalizer implements NormalizerInterface
{

    /**
     * @inheritDoc
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof InfrastructureFamily;
    }

    /**
     * @inheritDoc
     */
    public function normalize(mixed $object, string $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'name' => $object->getName(),
        ];
    }
}