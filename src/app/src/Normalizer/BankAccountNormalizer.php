<?php
namespace App\Normalizer;

use App\Entity\Player\BankAccount;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class BankAccountNormalizer implements NormalizerInterface
{

    /**
     * @inheritDoc
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof BankAccount;
    }

    /**
     * @inheritDoc
     */
    public function normalize(mixed $object, string $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'player' => $object->getPlayer()->getUsername(),
        ];
    }
}