<?php

namespace App\Normalizer;

use App\Entity\RidingClub;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class RidingClubNormalizer implements NormalizerInterface
{

    /**
     * @inheritDoc
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof RidingClub;
    }

    /**
     * @inheritDoc
     */
    public function normalize(mixed $object, string $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'owner' => $object->getOwner()->getUsername(),
            'capacity' => $object->getCapacity(),
            'membership_price' => $object->getMembershipPrice(),
        ];
    }
}