<?php
namespace App\Normalizer;

use App\Entity\ItemType;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ItemTypeNormalizer implements NormalizerInterface
{

    /**
     * @inheritDoc
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof ItemType;
    }

    /**
     * @inheritDoc
     */
    public function normalize(mixed $object, string $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'name' => $object->getName(),
            'description' => $object->getDescription(),
        ];
    }
}