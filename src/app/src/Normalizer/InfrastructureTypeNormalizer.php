<?php

namespace App\Normalizer;

use App\Entity\InfrastructureType;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class InfrastructureTypeNormalizer implements NormalizerInterface
{

    /**
     * @inheritDoc
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof InfrastructureType;
    }

    /**
     * @inheritDoc
     */
    public function normalize(mixed $object, string $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'name' => $object->getName(),
        ];
    }
}