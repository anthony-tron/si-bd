<?php
namespace App\Normalizer;

use App\Entity\Item;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ItemNormalizer implements NormalizerInterface
{

    /**
     * @inheritDoc
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof Item;
    }

    /**
     * @inheritDoc
     */
    public function normalize(mixed $object, string $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'name' => $object->getName(),
            'type' => $object->getType()->getName(),
            'family' => $object->getFamily()->getName(),
            'level' => $object->getLevel(),
            'value' => $object->getValue(),
        ];
    }
}