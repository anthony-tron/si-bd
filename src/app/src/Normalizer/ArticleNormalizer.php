<?php
namespace App\Normalizer;

use App\Entity\Newspaper\Article;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ArticleNormalizer implements NormalizerInterface
{

    /**
     * @inheritDoc
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof Article;
    }

    /**
     * @inheritDoc
     */
    public function normalize(mixed $object, string $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'newspaper' => $object->getNewspaper()->getId(),
            'title' => $object->getTitle(),
            'media' => $object->getMedia()?->getId(),
            'createdAt' => $object->getCreatedAt()->format('Y-m-d'),
            'content' => $object->getContent(),
        ];
    }
}