<?php

namespace App\Normalizer;

use App\Entity\Player\Player;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class PlayerNormalizer implements NormalizerInterface
{
    /**
     * @inheritDoc
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof Player;
    }

    /**
     * @inheritDoc
     */
    public function normalize(mixed $object, string $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'username' => $object->getUsername(),
            'email' => $object->getEmail(),
            'first_name' => $object->getFirstName(),
            'last_name' => $object->getLastName(),
            'gender' => $object->getGender(),
            'role' => $object->getRole(),
            'birthday' => $object->getBirthday()->format('d/m/Y'),
            'phone' => $object->getPhone(),
            'address' => $object->getAddress(),
            'website' => $object->getWebSite(),
            'registered_at' =>$object->getRegisteredAt()->format('d/m/Y'),
            'lastly_connected_at' => $object->getLastlyConnectedAt()?->format('d/m/Y'),
            'ip' => stream_get_contents($object->getIp()),
        ];
    }
}