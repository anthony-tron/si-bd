<?php
namespace App\Normalizer;

use App\Entity\Player\Transaction;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class TransactionNormalizer implements NormalizerInterface
{

    /**
     * @inheritDoc
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof Transaction;
    }

    /**
     * @inheritDoc
     */
    public function normalize(mixed $object, string $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'bank_account' => $object->getBankAccount()->getId(),
            'message' => $object->getMessage(),
            'sum' => $object->getSum(),
            'created_at' => $object->getCreatedAt()->format('Y-m-d'),
        ];
    }
}