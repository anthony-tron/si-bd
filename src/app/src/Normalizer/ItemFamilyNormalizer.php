<?php
namespace App\Normalizer;

use App\Entity\ItemFamily;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ItemFamilyNormalizer implements NormalizerInterface
{

    /**
     * @inheritDoc
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof ItemFamily;
    }

    /**
     * @inheritDoc
     */
    public function normalize(mixed $object, string $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'name' => $object->getName(),
        ];
    }
}