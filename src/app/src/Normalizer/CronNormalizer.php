<?php
namespace App\Normalizer;

use App\Entity\Cron;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class CronNormalizer implements NormalizerInterface
{

    /**
     * @inheritDoc
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof Cron;
    }

    /**
     * @inheritDoc
     */
    public function normalize(mixed $object, string $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'action' => $object->getAction()->getName(),
            'player' => $object->getPlayer()->getUsername(),
            'frequency' => $object->getFrequency(),
        ];
    }
}