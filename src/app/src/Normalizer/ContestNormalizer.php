<?php
namespace App\Normalizer;

use App\Entity\Contest\Contest;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ContestNormalizer implements NormalizerInterface
{

    /**
     * @inheritDoc
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof Contest;
    }

    /**
     * @inheritDoc
     */
    public function normalize(mixed $object, string $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'circuit' => $object->getCircuit()->getName(),
            'riding_club' => $object->getRidingClub()->getId(),
            'begin_date' => $object->getBeginDate()->format('Y-m-d'),
            'end_date' => $object->getEndDate()->format('Y-m-d'),
        ];
    }
}