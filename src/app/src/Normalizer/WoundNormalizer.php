<?php
namespace App\Normalizer;

use App\Entity\Horse\Wound;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class WoundNormalizer implements NormalizerInterface
{

    /**
     * @inheritDoc
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof Wound;
    }

    /**
     * @inheritDoc
     */
    public function normalize(mixed $object, string $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'name' => $object->getName(),
        ];
    }
}