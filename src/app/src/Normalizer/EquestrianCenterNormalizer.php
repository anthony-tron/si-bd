<?php
namespace App\Normalizer;

use App\Entity\EquestrianCenter;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class EquestrianCenterNormalizer implements NormalizerInterface
{

    /**
     * @inheritDoc
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof EquestrianCenter;
    }

    /**
     * @inheritDoc
     */
    public function normalize(mixed $object, string $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'owner' => $object->getOwner()->getUsername(),
            'capacity' => $object->getCapacity(),
        ];
    }
}