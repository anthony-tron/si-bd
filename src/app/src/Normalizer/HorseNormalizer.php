<?php
namespace App\Normalizer;

use App\Entity\Horse\Horse;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class HorseNormalizer implements NormalizerInterface
{

    /**
     * @inheritDoc
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof Horse;
    }

    /**
     * @inheritDoc
     */
    public function normalize(mixed $object, string $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'name' => $object->getName(),
            'breed' => $object->getBreed()->getName(),
            'player' => $object->getPlayer()->getUsername(),
            'resistance' => $object->getResistance(),
            'stamina' => $object->getStamina(),
            'spring' => $object->getSpring(),
            'sociability' => $object->getSociability(),
            'intelligence' => $object->getIntelligence(),
            'temperament' => $object->getTemperament(),
            'health' => $object->getHealth(),
            'morale' => $object->getMorale(),
            'stress' => $object->getStress(),
            'tiredness' => $object->getTiredness(),
            'hunger' => $object->getHunger(),
            'cleanliness' => $object->getCleanliness(),
        ];
    }
}