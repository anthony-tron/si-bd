<?php
namespace App\Normalizer;

use App\Entity\Newspaper\Newspaper;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class NewspaperNormalizer implements NormalizerInterface
{

    /**
     * @inheritDoc
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof Newspaper;
    }

    /**
     * @inheritDoc
     */
    public function normalize(mixed $object, string $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'date' => $object->getDate()->format('Y-m-d'),
        ];
    }
}