<?php
namespace App\Normalizer;

use App\Entity\Horse\Breed;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class BreedNormalizer implements NormalizerInterface
{

    /**
     * @inheritDoc
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof Breed;
    }

    /**
     * @inheritDoc
     */
    public function normalize(mixed $object, string $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'name' => $object->getName(),
            'description' => $object->getDescription(),
        ];
    }
}