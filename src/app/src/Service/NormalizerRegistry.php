<?php

namespace App\Service;

use App\Entity\Action;
use App\Entity\Contest\Circuit;
use App\Entity\Contest\Contest;
use App\Entity\Contest\ContestItem;
use App\Entity\Cron;
use App\Entity\EquestrianCenter;
use App\Entity\Horse\Breed;
use App\Entity\Horse\Disease;
use App\Entity\Horse\Horse;
use App\Entity\Horse\Parasite;
use App\Entity\Horse\Wound;
use App\Entity\Infrastructure;
use App\Entity\InfrastructureFamily;
use App\Entity\InfrastructureType;
use App\Entity\Item;
use App\Entity\ItemFamily;
use App\Entity\ItemType;
use App\Entity\Media;
use App\Entity\Newspaper\Article;
use App\Entity\Newspaper\Newspaper;
use App\Entity\Player\BankAccount;
use App\Entity\Player\Player;
use App\Entity\Player\Transaction;
use App\Entity\RidingClub;
use App\Normalizer\ActionNormalizer;
use App\Normalizer\ArticleNormalizer;
use App\Normalizer\BankAccountNormalizer;
use App\Normalizer\BreedNormalizer;
use App\Normalizer\CircuitNormalizer;
use App\Normalizer\ContestItemNormalizer;
use App\Normalizer\ContestNormalizer;
use App\Normalizer\CronNormalizer;
use App\Normalizer\DiseaseNormalizer;
use App\Normalizer\EquestrianCenterNormalizer;
use App\Normalizer\HorseNormalizer;
use App\Normalizer\InfrastructureFamilyNormalizer;
use App\Normalizer\InfrastructureNormalizer;
use App\Normalizer\InfrastructureTypeNormalizer;
use App\Normalizer\ItemFamilyNormalizer;
use App\Normalizer\ItemNormalizer;
use App\Normalizer\ItemTypeNormalizer;
use App\Normalizer\MediaNormalizer;
use App\Normalizer\NewspaperNormalizer;
use App\Normalizer\ParasiteNormalizer;
use App\Normalizer\PlayerNormalizer;
use App\Normalizer\TransactionNormalizer;
use App\Normalizer\WoundNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class NormalizerRegistry
{
    const map = [
        Player::class => PlayerNormalizer::class,
        Horse::class => HorseNormalizer::class,
        Action::class => ActionNormalizer::class,
        Cron::class => CronNormalizer::class,
        EquestrianCenter::class => EquestrianCenterNormalizer::class,
        Infrastructure::class => InfrastructureNormalizer::class,
        InfrastructureType::class => InfrastructureTypeNormalizer::class,
        InfrastructureFamily::class => InfrastructureFamilyNormalizer::class,
        Item::class => ItemNormalizer::class,
        ItemType::class => ItemTypeNormalizer::class,
        ItemFamily::class => ItemFamilyNormalizer::class,
        RidingClub::class => RidingClub::class,
        Circuit::class => CircuitNormalizer::class,
        Contest::class => ContestNormalizer::class,
        ContestItem::class => ContestItemNormalizer::class,
        Breed::class => BreedNormalizer::class,
        Disease::class => DiseaseNormalizer::class,
        Parasite::class => ParasiteNormalizer::class,
        Wound::class => WoundNormalizer::class,
        Article::class => ArticleNormalizer::class,
        Newspaper::class => NewspaperNormalizer::class,
        BankAccount::class => BankAccountNormalizer::class,
        Media::class => MediaNormalizer::class,
        Transaction::class => TransactionNormalizer::class,
    ];

    public function getNormalizer(string $id): NormalizerInterface {
        $class = self::map[$id]
            ?? throw new \ValueError("No normalizer supports for $id.");

        return new $class();
    }
}