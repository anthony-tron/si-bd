<?php

namespace App\Service;

class EntityRegistry
{
    /**
     * @param string $entityClass
     * @return string the full class of the entity
     */
    public function resolveEntityClass(string $entityClass): string
    {
        $className = "App\\Entity\\" . ucfirst($entityClass);
        if (class_exists($className)) return $className;

        $className = "App\\Entity\\Contest\\" . ucfirst($entityClass);
        if (class_exists($className)) return $className;

        $className = "App\\Entity\\Horse\\" . ucfirst($entityClass);
        if (class_exists($className)) return $className;

        $className = "App\\Entity\\Newspaper\\" . ucfirst($entityClass);
        if (class_exists($className)) return $className;

        $className = "App\\Entity\\Player\\" . ucfirst($entityClass);
        if (class_exists($className)) return $className;

        throw new \ValueError("Could not resolve class $entityClass.");
    }
}