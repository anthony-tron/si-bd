<?php

namespace App\Service;

class ListBuilder
{
    private $repository;
    private $maxResults;
    private $page;
    private $normalizer;

    public function setRepository($repository) {
        $this->repository = $repository;
        return $this;
    }

    public function setMaxResults($setMaxResults) {
        $this->maxResults = $setMaxResults;
        return $this;
    }

    public function setPage(int $page) {
        $this->page = $page;
        return $this;
    }

    public function setNormalizer($normalizer) {
        $this->normalizer = $normalizer;
        return $this;
    }

    public function getObjects(): array {
        $objects = $this->repository
                ->createQueryBuilder('t')
                ->setMaxResults($this->maxResults)
                ->setFirstResult($this->page * $this->maxResults)
                ->getQuery()
                ->getResult();

        return array_map(
            fn($e) => $this->normalizer->normalize($e),
            $objects
        );
    }

}