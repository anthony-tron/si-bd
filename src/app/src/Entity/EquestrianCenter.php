<?php

namespace App\Entity;

use App\Entity\Player\Player;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * EquestrianCenter
 *
 * @ORM\Table(name="equestrian_center", indexes={@ORM\Index(name="equestrian_center_player_id_fk", columns={"owner"})})
 * @ORM\Entity(repositoryClass="App\Repository\EquestrianCenterRepository")
 */
class EquestrianCenter
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="capacity", type="integer", nullable=false)
     */
    private $capacity;

    /**
     * @var Player
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Player\Player")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="owner", referencedColumnName="id")
     * })
     */
    private $owner;

    /**
     * Many Users have Many Groups.
     * @ORM\ManyToMany(targetEntity="Cron")
     * @ORM\JoinTable(name="cron_equestrian_center",
     *      joinColumns={@ORM\JoinColumn(name="equestrian_center", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="cron", referencedColumnName="id")}
     *      )
     */
    private $cron;

    /**
     * @ORM\ManyToMany(targetEntity="Infrastructure")
     * @ORM\JoinTable(name="equestrian_center_infrastructure",
     *      joinColumns={@ORM\JoinColumn(name="equestrian_center", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="infrastructure", referencedColumnName="id", unique=true)}
     *      )
     */
    private $infrastructure;

    public function __construct()
    {
        $this->cron = new ArrayCollection();
        $this->infrastructure = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(int $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getOwner(): ?Player
    {
        return $this->owner;
    }

    public function setOwner(?Player $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection<int, Cron>
     */
    public function getCron(): Collection
    {
        return $this->cron;
    }

    public function addCron(Cron $cron): self
    {
        if (!$this->cron->contains($cron)) {
            $this->cron[] = $cron;
        }

        return $this;
    }

    public function removeCron(Cron $cron): self
    {
        $this->cron->removeElement($cron);

        return $this;
    }

    /**
     * @return Collection<int, Infrastructure>
     */
    public function getInfrastructure(): Collection
    {
        return $this->infrastructure;
    }

    public function addInfrastructure(Infrastructure $infrastructure): self
    {
        if (!$this->infrastructure->contains($infrastructure)) {
            $this->infrastructure[] = $infrastructure;
        }

        return $this;
    }

    public function removeInfrastructure(Infrastructure $infrastructure): self
    {
        $this->infrastructure->removeElement($infrastructure);

        return $this;
    }


}
