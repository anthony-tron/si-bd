<?php

namespace App\Entity;

use App\Entity\Player\Player;
use Doctrine\ORM\Mapping as ORM;

/**
 * Cron
 *
 * @ORM\Table(name="cron", indexes={@ORM\Index(name="cron_action_id_fk", columns={"action"}), @ORM\Index(name="cron_player_id_fk", columns={"Player"})})
 * @ORM\Entity(repositoryClass="App\Repository\CronRepository")
 */
class Cron
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="frequency", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $frequency = NULL;

    /**
     * @var Action
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Action")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="action", referencedColumnName="id")
     * })
     */
    private $action;

    /**
     * @var Player
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Player\Player")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Player", referencedColumnName="id")
     * })
     */
    private $player;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFrequency(): ?int
    {
        return $this->frequency;
    }

    public function setFrequency(?int $frequency): self
    {
        $this->frequency = $frequency;

        return $this;
    }

    public function getAction(): ?Action
    {
        return $this->action;
    }

    public function setAction(?Action $action): self
    {
        $this->action = $action;

        return $this;
    }

    public function getPlayer(): ?Player
    {
        return $this->player;
    }

    public function setPlayer(?Player $player): self
    {
        $this->player = $player;

        return $this;
    }


}
