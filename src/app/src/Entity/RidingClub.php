<?php

namespace App\Entity;

use App\Entity\Player\Player;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * RidingClub
 *
 * @ORM\Table(name="riding_club", indexes={@ORM\Index(name="riding_club_player_id_fk", columns={"owner"})})
 * @ORM\Entity(repositoryClass="App\Repository\RidingClubRepository")
 */
class RidingClub
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="capacity", type="integer", nullable=false)
     */
    private $capacity;

    /**
     * @var int
     *
     * @ORM\Column(name="membership_price", type="integer", nullable=false)
     */
    private $membershipPrice = '0';

    /**
     * @var Player
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Player\Player")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="owner", referencedColumnName="id")
     * })
     */
    private $owner;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Player\Player")
     * @ORM\JoinTable(name="riding_club_player",
     *      joinColumns={@ORM\JoinColumn(name="riding_club", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="player", referencedColumnName="id")}
     *      )
     */
    private $player;

    /**
     * @ORM\ManyToMany(targetEntity="Infrastructure")
     * @ORM\JoinTable(name="riding_club_infrastructure",
     *      joinColumns={@ORM\JoinColumn(name="riding_club", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="infrastructure", referencedColumnName="id", unique=true)}
     *      )
     */
    private $infrastructure;

    public function __construct()
    {
        $this->player = new ArrayCollection();
        $this->infrastructure = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(int $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getMembershipPrice(): ?int
    {
        return $this->membershipPrice;
    }

    public function setMembershipPrice(int $membershipPrice): self
    {
        $this->membershipPrice = $membershipPrice;

        return $this;
    }

    public function getOwner(): ?Player
    {
        return $this->owner;
    }

    public function setOwner(?Player $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection<int, Player>
     */
    public function getPlayer(): Collection
    {
        return $this->player;
    }

    public function addPlayer(Player $player): self
    {
        if (!$this->player->contains($player)) {
            $this->player[] = $player;
        }

        return $this;
    }

    public function removePlayer(Player $player): self
    {
        $this->player->removeElement($player);

        return $this;
    }

    /**
     * @return Collection<int, Infrastructure>
     */
    public function getInfrastructure(): Collection
    {
        return $this->infrastructure;
    }

    public function addInfrastructure(Infrastructure $infrastructure): self
    {
        if (!$this->infrastructure->contains($infrastructure)) {
            $this->infrastructure[] = $infrastructure;
        }

        return $this;
    }

    public function removeInfrastructure(Infrastructure $infrastructure): self
    {
        $this->infrastructure->removeElement($infrastructure);

        return $this;
    }
}
