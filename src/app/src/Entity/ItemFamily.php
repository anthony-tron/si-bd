<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItemFamily
 *
 * @ORM\Table(name="item_family")
 * @ORM\Entity(repositoryClass="App\Repository\ItemFamilyRepository")
 */
class ItemFamily
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=31, nullable=true, options={"default"="NULL"})
     */
    private $name = 'NULL';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
