<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfrastructureFamily
 *
 * @ORM\Table(name="infrastructure_family")
 * @ORM\Entity(repositoryClass="App\Repository\InfrastructureFamilyRepository")
 */
class InfrastructureFamily
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=40, nullable=false)
     */
    private $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


}
