<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Infrastructure
 *
 * @ORM\Table(name="infrastructure", indexes={@ORM\Index(name="infrastructure_infrastructure_family_id_fk", columns={"family"}), @ORM\Index(name="infrastructure_infrastructure_type_id_fk", columns={"type"})})
 * @ORM\Entity(repositoryClass="App\Repository\InfrastructureRepository")
 */
class Infrastructure
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="resources", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $resources = NULL;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="item_capacity", type="integer", nullable=false)
     */
    private $itemCapacity;

    /**
     * @var int
     *
     * @ORM\Column(name="horse_capacity", type="integer", nullable=false)
     */
    private $horseCapacity;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer", nullable=false)
     */
    private $price;

    /**
     * @var int
     *
     * @ORM\Column(name="level", type="integer", nullable=false)
     */
    private $level;

    /**
     * @var InfrastructureFamily
     *
     * @ORM\ManyToOne(targetEntity="InfrastructureFamily")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="family", referencedColumnName="id")
     * })
     */
    private $family;

    /**
     * @var InfrastructureType
     *
     * @ORM\ManyToOne(targetEntity="InfrastructureType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type", referencedColumnName="id")
     * })
     */
    private $type;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Horse\Horse")
     * @ORM\JoinTable(name="infrastructure_horse",
     *      joinColumns={@ORM\JoinColumn(name="infrastructure", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="horse", referencedColumnName="id", unique=true)}
     *      )
     */
    private $horse;

    /**
     * @ORM\ManyToMany(targetEntity="Item")
     * @ORM\JoinTable(name="infrastructure_item",
     *      joinColumns={@ORM\JoinColumn(name="infrastructure", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="item", referencedColumnName="id", unique=true)}
     *      )
     */
    private $item;

    public function __construct()
    {
        $this->horse = new ArrayCollection();
        $this->item = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getResources(): ?int
    {
        return $this->resources;
    }

    public function setResources(?int $resources): self
    {
        $this->resources = $resources;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getItemCapacity(): ?int
    {
        return $this->itemCapacity;
    }

    public function setItemCapacity(int $itemCapacity): self
    {
        $this->itemCapacity = $itemCapacity;

        return $this;
    }

    public function getHorseCapacity(): ?int
    {
        return $this->horseCapacity;
    }

    public function setHorseCapacity(int $horseCapacity): self
    {
        $this->horseCapacity = $horseCapacity;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getFamily(): ?InfrastructureFamily
    {
        return $this->family;
    }

    public function setFamily(?InfrastructureFamily $family): self
    {
        $this->family = $family;

        return $this;
    }

    public function getType(): ?InfrastructureType
    {
        return $this->type;
    }

    public function setType(?InfrastructureType $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection<int, Horse>
     */
    public function getHorse(): Collection
    {
        return $this->horse;
    }

    public function addHorse(Horse $horse): self
    {
        if (!$this->horse->contains($horse)) {
            $this->horse[] = $horse;
        }

        return $this;
    }

    public function removeHorse(Horse $horse): self
    {
        $this->horse->removeElement($horse);

        return $this;
    }

    /**
     * @return Collection<int, Item>
     */
    public function getItem(): Collection
    {
        return $this->item;
    }

    public function addItem(Item $item): self
    {
        if (!$this->item->contains($item)) {
            $this->item[] = $item;
        }

        return $this;
    }

    public function removeItem(Item $item): self
    {
        $this->item->removeElement($item);

        return $this;
    }

}
