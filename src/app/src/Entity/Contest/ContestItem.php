<?php

namespace App\Entity\Contest;

use App\Entity\Item;
use Doctrine\ORM\Mapping as ORM;

/**
 * ContestItem
 *
 * @ORM\Table(name="contest_item", uniqueConstraints={@ORM\UniqueConstraint(name="contest_item_item_id_fk", columns={"item"})}, indexes={@ORM\Index(name="contest_item_contest_id_fk", columns={"contest"})})
 * @ORM\Entity(repositoryClass="App\Repository\ContestItemRepository")
 */
class ContestItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="rank", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $rank = NULL;

    /**
     * @var Contest
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Contest\Contest")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contest", referencedColumnName="id")
     * })
     */
    private $contest;

    /**
     * @var Item
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Item")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="item", referencedColumnName="id")
     * })
     */
    private $item;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRank(): ?int
    {
        return $this->rank;
    }

    public function setRank(?int $rank): self
    {
        $this->rank = $rank;

        return $this;
    }

    public function getContest(): ?Contest
    {
        return $this->contest;
    }

    public function setContest(?Contest $contest): self
    {
        $this->contest = $contest;

        return $this;
    }

    public function getItem(): ?Item
    {
        return $this->item;
    }

    public function setItem(?Item $item): self
    {
        $this->item = $item;

        return $this;
    }


}
