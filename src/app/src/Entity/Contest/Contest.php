<?php

namespace App\Entity\Contest;

use App\Entity\RidingClub;
use Doctrine\ORM\Mapping as ORM;

/**
 * Contest
 *
 * @ORM\Table(name="contest", indexes={@ORM\Index(name="contest_circuit_id_fk", columns={"circuit"}), @ORM\Index(name="contest_riding_club_id_fk", columns={"riding_club"})})
 * @ORM\Entity(repositoryClass="App\Repository\ContestRepository")
 */
class Contest
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="begin_date", type="date", nullable=false)
     */
    private $beginDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="date", nullable=false)
     */
    private $endDate;

    /**
     * @var int|null
     *
     * @ORM\Column(name="infrastructure", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $infrastructure = NULL;

    /**
     * @var Circuit
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Contest\Circuit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="circuit", referencedColumnName="id")
     * })
     */
    private $circuit;

    /**
     * @var RidingClub
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\RidingClub")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="riding_club", referencedColumnName="id")
     * })
     */
    private $ridingClub;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBeginDate(): ?\DateTimeInterface
    {
        return $this->beginDate;
    }

    public function setBeginDate(\DateTimeInterface $beginDate): self
    {
        $this->beginDate = $beginDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getInfrastructure(): ?int
    {
        return $this->infrastructure;
    }

    public function setInfrastructure(?int $infrastructure): self
    {
        $this->infrastructure = $infrastructure;

        return $this;
    }

    public function getCircuit(): ?Circuit
    {
        return $this->circuit;
    }

    public function setCircuit(?Circuit $circuit): self
    {
        $this->circuit = $circuit;

        return $this;
    }

    public function getRidingClub(): ?RidingClub
    {
        return $this->ridingClub;
    }

    public function setRidingClub(?RidingClub $ridingClub): self
    {
        $this->ridingClub = $ridingClub;

        return $this;
    }


}
