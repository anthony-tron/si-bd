<?php

namespace App\Entity\Horse;

use App\Entity\Item;
use App\Entity\Player\Player;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Horse
 *
 * @ORM\Table(name="horse", indexes={@ORM\Index(name="horse_breed_id_fk", columns={"breed"}), @ORM\Index(name="horse_player_id_fk", columns={"Player"})})
 * @ORM\Entity(repositoryClass="App\Repository\HorseRepository")
 */
class Horse
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="resistance", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $resistance = NULL;

    /**
     * @var int|null
     *
     * @ORM\Column(name="stamina", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $stamina = NULL;

    /**
     * @var int|null
     *
     * @ORM\Column(name="spring", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $spring = NULL;

    /**
     * @var int|null
     *
     * @ORM\Column(name="speed", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $speed = NULL;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sociability", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $sociability = NULL;

    /**
     * @var int|null
     *
     * @ORM\Column(name="intelligence", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $intelligence = NULL;

    /**
     * @var int|null
     *
     * @ORM\Column(name="temperament", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $temperament = NULL;

    /**
     * @var int|null
     *
     * @ORM\Column(name="health", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $health = NULL;

    /**
     * @var int|null
     *
     * @ORM\Column(name="morale", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $morale = NULL;

    /**
     * @var int|null
     *
     * @ORM\Column(name="stress", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $stress = NULL;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tiredness", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $tiredness = NULL;

    /**
     * @var int|null
     *
     * @ORM\Column(name="hunger", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $hunger = NULL;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cleanliness", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $cleanliness = NULL;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=31, nullable=false)
     */
    private $name;

    /**
     * @var Breed
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Horse\Breed")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="breed", referencedColumnName="id")
     * })
     */
    private $breed;

    /**
     * @var Player
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Player\Player")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Player", referencedColumnName="id")
     * })
     */
    private $player;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Item")
     * @ORM\JoinTable(name="horse_item",
     *      joinColumns={@ORM\JoinColumn(name="horse", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="item", referencedColumnName="id", unique=true)}
     *      )
     */
    private $item;

    /**
     * Many Users have Many Groups.
     * @ORM\ManyToMany(targetEntity="App\Entity\Horse\Disease")
     * @ORM\JoinTable(name="horse_disease",
     *      joinColumns={@ORM\JoinColumn(name="horse", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="disease", referencedColumnName="id")}
     *      )
     */
    private $disease;

    /**
     * Many Users have Many Groups.
     * @ORM\ManyToMany(targetEntity="App\Entity\Horse\Parasite")
     * @ORM\JoinTable(name="horse_parasit",
     *      joinColumns={@ORM\JoinColumn(name="horse", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="parasite", referencedColumnName="id")}
     *      )
     */
    private $parasit;

    /**
     * Many Users have Many Groups.
     * @ORM\ManyToMany(targetEntity="App\Entity\Horse\Wound")
     * @ORM\JoinTable(name="horse_wound",
     *      joinColumns={@ORM\JoinColumn(name="horse", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="wound", referencedColumnName="id")}
     *      )
     */
    private $wound;


    public function __construct()
    {
        $this->item = new ArrayCollection();
        $this->disease = new ArrayCollection();
        $this->parasit = new ArrayCollection();
        $this->wound = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getResistance(): ?int
    {
        return $this->resistance;
    }

    public function setResistance(?int $resistance): self
    {
        $this->resistance = $resistance;

        return $this;
    }

    public function getStamina(): ?int
    {
        return $this->stamina;
    }

    public function setStamina(?int $stamina): self
    {
        $this->stamina = $stamina;

        return $this;
    }

    public function getSpring(): ?int
    {
        return $this->spring;
    }

    public function setSpring(?int $spring): self
    {
        $this->spring = $spring;

        return $this;
    }

    public function getSpeed(): ?int
    {
        return $this->speed;
    }

    public function setSpeed(?int $speed): self
    {
        $this->speed = $speed;

        return $this;
    }

    public function getSociability(): ?int
    {
        return $this->sociability;
    }

    public function setSociability(?int $sociability): self
    {
        $this->sociability = $sociability;

        return $this;
    }

    public function getIntelligence(): ?int
    {
        return $this->intelligence;
    }

    public function setIntelligence(?int $intelligence): self
    {
        $this->intelligence = $intelligence;

        return $this;
    }

    public function getTemperament(): ?int
    {
        return $this->temperament;
    }

    public function setTemperament(?int $temperament): self
    {
        $this->temperament = $temperament;

        return $this;
    }

    public function getHealth(): ?int
    {
        return $this->health;
    }

    public function setHealth(?int $health): self
    {
        $this->health = $health;

        return $this;
    }

    public function getMorale(): ?int
    {
        return $this->morale;
    }

    public function setMorale(?int $morale): self
    {
        $this->morale = $morale;

        return $this;
    }

    public function getStress(): ?int
    {
        return $this->stress;
    }

    public function setStress(?int $stress): self
    {
        $this->stress = $stress;

        return $this;
    }

    public function getTiredness(): ?int
    {
        return $this->tiredness;
    }

    public function setTiredness(?int $tiredness): self
    {
        $this->tiredness = $tiredness;

        return $this;
    }

    public function getHunger(): ?int
    {
        return $this->hunger;
    }

    public function setHunger(?int $hunger): self
    {
        $this->hunger = $hunger;

        return $this;
    }

    public function getCleanliness(): ?int
    {
        return $this->cleanliness;
    }

    public function setCleanliness(?int $cleanliness): self
    {
        $this->cleanliness = $cleanliness;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBreed(): ?Breed
    {
        return $this->breed;
    }

    public function setBreed(?Breed $breed): self
    {
        $this->breed = $breed;

        return $this;
    }

    public function getPlayer(): ?Player
    {
        return $this->player;
    }

    public function setPlayer(?Player $player): self
    {
        $this->player = $player;

        return $this;
    }

    /**
     * @return Collection<int, Item>
     */
    public function getItem(): Collection
    {
        return $this->item;
    }

    public function addItem(Item $item): self
    {
        if (!$this->item->contains($item)) {
            $this->item[] = $item;
        }

        return $this;
    }

    public function removeItem(Item $item): self
    {
        $this->item->removeElement($item);

        return $this;
    }

    /**
     * @return Collection<int, Disease>
     */
    public function getDisease(): Collection
    {
        return $this->disease;
    }

    public function addDisease(Disease $disease): self
    {
        if (!$this->disease->contains($disease)) {
            $this->disease[] = $disease;
        }

        return $this;
    }

    public function removeDisease(Disease $disease): self
    {
        $this->disease->removeElement($disease);

        return $this;
    }

    /**
     * @return Collection<int, Parasite>
     */
    public function getParasit(): Collection
    {
        return $this->parasit;
    }

    public function addParasit(Parasite $parasit): self
    {
        if (!$this->parasit->contains($parasit)) {
            $this->parasit[] = $parasit;
        }

        return $this;
    }

    public function removeParasit(Parasite $parasit): self
    {
        $this->parasit->removeElement($parasit);

        return $this;
    }

    /**
     * @return Collection<int, Wound>
     */
    public function getWound(): Collection
    {
        return $this->wound;
    }

    public function addWound(Wound $wound): self
    {
        if (!$this->wound->contains($wound)) {
            $this->wound[] = $wound;
        }

        return $this;
    }

    public function removeWound(Wound $wound): self
    {
        $this->wound->removeElement($wound);

        return $this;
    }


}
