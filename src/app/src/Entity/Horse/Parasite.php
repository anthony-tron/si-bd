<?php

namespace App\Entity\Horse;

use Doctrine\ORM\Mapping as ORM;

/**
 * Parasite
 *
 * @ORM\Table(name="parasite")
 * @ORM\Entity(repositoryClass="App\Repository\ParasiteRepository")
 */
class Parasite
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=63, nullable=true, options={"default"="NULL"})
     */
    private $name = 'NULL';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function __toString(): string
    {
        return $this->getName();
    }

}
