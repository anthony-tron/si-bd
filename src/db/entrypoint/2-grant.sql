BEGIN;

USE si;

CREATE USER `si_admin`
    IDENTIFIED BY '8uCnjY9pVkh6aUsVNnEzLK6Wud0NR5jK7onb123igE';

CREATE USER `grant_admin`
    IDENTIFIED BY 'y0byqo3LjvGNkvfDYuRKQR78pPUJ664OJVQoRstfg';

CREATE USER `performance_admin`
    IDENTIFIED BY 'cuzCNw6JdToiL2tb13nLbkNq19mNPjlzdE1TXWtFfI';

CREATE USER `cron_user`
    IDENTIFIED BY '3s8OwG4kJbrzXdc2z3tobedqL4Fz41ZXKIzsWVGJo';

CREATE USER `dev`
    IDENTIFIED BY '1s27vOTjVrgqRVL4AXEeLBBW6CFEUVx9R1hFVLmQk7g';

CREATE USER `community_manager`
    IDENTIFIED BY 'Q8JikbmA5RIiWIhgnEK3C7vCA09s7fPgmZ02eX41o0';

CREATE USER `horse_specialist`
    IDENTIFIED BY 'vs7S4rGjiE0uTpYRqdDMEwEq0ZwkySF8v6wbXiE';

CREATE USER `contest_specialist`
    IDENTIFIED BY 'PfkTrB5TOK1XozKcGef8LsoCyFKx4gv9InMVjRcv4';

CREATE USER `publisher`
    IDENTIFIED BY 'e8m987gQ0zU4G5gWkdRxYuQGrKtuz51cGMnvfkYBpM8';

CREATE USER `client`
    IDENTIFIED BY 'egvAQj1gZhu0G4TJRDDA5Ek2M7p6jlQl5Iz7qjmpXzI';

-- si_admin

GRANT ALL
    ON si.*
    TO `si_admin`;


-- grant_admin

GRANT GRANT OPTION
    ON *.*
    TO `grant_admin`;


-- performance_admin

GRANT INDEX
    ON *.*
    TO `performance_admin`;


-- cron_user

GRANT RELOAD, SHUTDOWN, PROCESS
    ON *.*
    TO `cron_user`;


-- dev
-- crud on all tables

GRANT SELECT, INSERT, UPDATE, DELETE
    ON si.*
    TO `dev`;


-- community_manager
-- modify player related data

GRANT SELECT, INSERT, UPDATE
    ON player
    TO `community_manager`;

GRANT SELECT, INSERT, UPDATE
    ON media
    TO `community_manager`;


-- horse_specialist

GRANT SELECT, UPDATE
    ON horse
    TO `horse_specialist`;

GRANT SELECT, UPDATE
    ON breed
    TO `horse_specialist`;

GRANT SELECT, UPDATE
    ON horse_disease
    TO `horse_specialist`;

GRANT SELECT, UPDATE
    ON horse_item
    TO `horse_specialist`;

GRANT SELECT, UPDATE
    ON horse_parasite
    TO `horse_specialist`;

GRANT SELECT, UPDATE
    ON horse_wound
    TO `horse_specialist`;


-- contest_specialist
-- access to contest related tables

GRANT SELECT, INSERT, UPDATE, DELETE
    ON contest
    TO `contest_specialist`;

GRANT SELECT, INSERT, UPDATE, DELETE
    ON contest_item
    TO `contest_specialist`;

GRANT SELECT, INSERT, UPDATE, DELETE
    ON circuit
    TO `contest_specialist`;


-- publisher
-- access to newspaper related tables

GRANT SELECT, INSERT, UPDATE, DELETE
    ON article
    TO `publisher`;

GRANT SELECT, INSERT, UPDATE, DELETE
    ON newspaper
    TO `publisher`;


-- client
-- access to newspaper related tables
-- access to contest related tables

GRANT SELECT
    ON contest
    TO `client`;

GRANT SELECT
    ON contest_item
    TO `client`;

GRANT SELECT
    ON article
    TO `client`;

GRANT SELECT
    ON newspaper
    TO `client`;

GRANT SELECT
    ON circuit
    TO `client`;


COMMIT;
