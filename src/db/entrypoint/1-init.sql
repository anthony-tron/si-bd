DELETE FROM mysql.user
    WHERE Host <> '%'
    AND user='root';

create schema si collate utf8_general_ci;

USE si;

create or replace table action
(
    id   int auto_increment
        primary key,
    name varchar(31) not null
);

create or replace table breed
(
    id          int auto_increment
        primary key,
    name        varchar(31)  not null,
    description varchar(255) not null
);

create or replace table circuit
(
    id   int         not null
        primary key,
    name varchar(16) not null
);

create or replace table disease
(
    id   int auto_increment
        primary key,
    name varchar(63) not null
);

create or replace table infrastructure_family
(
    id   int         not null
        primary key,
    name varchar(40) not null
);

create or replace table infrastructure_type
(
    id   int         not null
        primary key,
    name varchar(40) not null
);

create or replace table infrastructure
(
    id             int auto_increment
        primary key,
    type           int          not null,
    family         int          not null,
    resources      int          null,
    description    varchar(255) not null,
    item_capacity  int          not null,
    horse_capacity int          not null,
    price          int          not null,
    level          int          not null,
    constraint infrastructure_infrastructure_family_id_fk
        foreign key (family) references infrastructure_family (id),
    constraint infrastructure_infrastructure_type_id_fk
        foreign key (type) references infrastructure_type (id)
);

create or replace table item_family
(
    id   int auto_increment
        primary key,
    name varchar(31) null
);

create or replace table item_type
(
    id          int auto_increment
        primary key,
    name        varchar(31)  not null,
    description varchar(127) not null
);

create or replace table item
(
    id     int auto_increment
        primary key,
    name   varchar(31) not null,
    level  int         not null,
    value  int         not null,
    type   int         not null,
    family int         not null,
    constraint item_item_family_id_fk
        foreign key (family) references item_family (id),
    constraint item_type_id_fk
        foreign key (type) references item_type (id)
);

create or replace table infrastructure_item
(
    id             int not null
        primary key,
    infrastructure int not null,
    item           int not null,
    constraint infrastructure_item_id_uindex
        unique (id),
    constraint infrastructure_item_item_uindex
        unique (item),
    constraint infrastructure_item_infrastructure_id_fk
        foreign key (infrastructure) references infrastructure (id),
    constraint infrastructure_item_item_id_fk
        foreign key (item) references item (id)
);

create or replace table media
(
    id   int auto_increment
        primary key,
    path varchar(255) not null
);

create or replace table newspaper
(
    id   int auto_increment
        primary key,
    date date not null
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

create or replace table article
(
    id         int auto_increment
        primary key,
    content    varchar(1023)                         null,
    media      int                                   null,
    created_at timestamp default current_timestamp() null,
    title      varchar(63)                           null,
    newspaper  int null,
    constraint article_newspaper_id_fk
        foreign key (newspaper) references newspaper (id)

) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

# create or replace table newspaper_article
# (
#     id        int auto_increment
#         primary key,
#     newspaper int null,
#     article   int null UNIQUE,
#     constraint newspaper_article_article_id_fk
#         foreign key (article) references article (id),
#     constraint newspaper_article_newspaper_id_fk
#         foreign key (newspaper) references newspaper (id)
# );

create or replace table parasite
(
    id   int auto_increment
        primary key,
    name varchar(63) null
);

create or replace table player
(
    id                  int auto_increment
        primary key,
    username            varchar(15)                           not null,
    email               varchar(320)                          not null,
    password            varchar(60)                           not null,
    first_name          varchar(31)                           not null,
    last_name           varchar(31)                           not null,
    gender              enum ('MALE', 'FEMALE', 'NON_BINARY') not null,
    birthday            date                                  not null,
    phone               varchar(15)                           null comment 'Recommendation E.164',
    address             varchar(63)                           null,
    description         varchar(511)                          null,
    web_site            varchar(127)                          null,
    registered_at       timestamp default current_timestamp() not null,
    lastly_connected_at datetime                              null,
    media               int                                   null,
    ip                  varbinary(16)                         not null comment 'Use with INET_ATON, INET_NTOA (IPv4)
or INET6_ATON, INET6_NTOA (IPv6).',
    role                enum ('ROLE_USER', 'ROLE_ADMIN', 'ROLE_PUBLISHER', 'ROLE_CLIENT', 'ROLE_HORSE_SPECIALIST', 'ROLE_CONTEST_SPECIALIST', 'ROLE_DEV', 'ROLE_PERFORMANCE', 'ROLE_CRON', 'ROLE_GRANT')      not null default 'ROLE_USER',
        constraint player_email_uindex
        unique (email),
    constraint player_username_uindex
        unique (username),
    constraint player_media_id_fk
        foreign key (media) references media (id)
);

create or replace table bank_account
(
    id     int auto_increment
        primary key,
    player int not null,
    constraint bank_account_2_player_uindex
        unique (player),
    constraint bank_account_2_player_id_fk
        foreign key (player) references player (id)
);

create or replace table cron
(
    id        int auto_increment
        primary key,
    action    int null,
    frequency int null,
    player    int not null,
    constraint cron_action_id_fk
        foreign key (action) references action (id),
    constraint cron_player_id_fk
        foreign key (player) references player (id)
);

create or replace table equestrian_center
(
    id       int auto_increment
        primary key,
    owner    int not null,
    capacity int not null,
    constraint equestrian_center_player_id_fk
        foreign key (owner) references player (id)
);

create or replace table cron_equestrian_center
(
    id                int not null
        primary key,
    cron              int null,
    equestrian_center int null,
    constraint cron_equestrian_center_cron_id_fk
        foreign key (cron) references cron (id)
            on delete cascade,
    constraint cron_equestrian_center_equestrian_center_id_fk
        foreign key (equestrian_center) references equestrian_center (id)
);

create or replace table equestrian_center_infrastructure
(
    id                int auto_increment
        primary key,
    equestrian_center int not null,
    infrastructure    int not null UNIQUE,
    constraint equestrian_center_infrastructure_equestrian_center_id_fk
        foreign key (equestrian_center) references equestrian_center (id),
    constraint equestrian_center_infrastructure_infrastructure_id_fk
        foreign key (infrastructure) references infrastructure (id)
);

create or replace table horse
(
    id           int auto_increment
        primary key,
    resistance   int         null,
    stamina      int         null,
    spring       int         null,
    speed        int         null,
    sociability  int         null,
    intelligence int         null,
    temperament  int         null,
    health       int         null,
    morale       int         null,
    stress       int         null,
    tiredness    int         null,
    hunger       int         null,
    cleanliness  int         null,
    breed        int         not null,
    player       int         null,
    name         varchar(31) not null,
    constraint horse_breed_id_fk
        foreign key (breed) references breed (id),
    constraint horse_player_id_fk
        foreign key (player) references player (id)
);

create or replace table horse_disease
(
    id      int auto_increment
        primary key,
    horse   int null,
    disease int null,
    constraint horse_disease_disease_id_fk
        foreign key (disease) references disease (id),
    constraint horse_disease_horse_id_fk
        foreign key (horse) references horse (id)
);

create or replace table horse_item
(
    id    int auto_increment
        primary key,
    horse int null,
    item  int null,
    constraint horse_item_item_uindex
        unique (item),
    constraint horse_item_horse_id_fk
        foreign key (horse) references horse (id),
    constraint horse_item_item_id_fk
        foreign key (item) references item (id)
);

create or replace table horse_parasite
(
    id       int auto_increment
        primary key,
    horse    int null,
    parasite int null,
    constraint horse_parasite_horse_id_fk
        foreign key (horse) references horse (id),
    constraint horse_parasite_parasite_id_fk
        foreign key (parasite) references parasite (id)
);

create or replace table infrastructure_horse
(
    id             int auto_increment
        primary key,
    infrastructure int not null,
    horse          int not null,
    constraint infrastructure_horse_horse_uindex
        unique (horse),
    constraint infrastructure_horse_horse_id_fk
        foreign key (horse) references horse (id),
    constraint infrastructure_horse_infrastructure_id_fk
        foreign key (infrastructure) references infrastructure (id)
);

create or replace table riding_club
(
    id               int auto_increment
        primary key,
    owner            int           not null,
    capacity         int           not null,
    membership_price int default 0 not null,
    constraint riding_club_player_id_fk
        foreign key (owner) references player (id)
);

create or replace table contest
(
    id             int auto_increment
        primary key,
    begin_date     date not null,
    end_date       date not null,
    infrastructure int  null,
    riding_club    int  not null,
    circuit        int  null,
    constraint contest_circuit_id_fk
        foreign key (circuit) references circuit (id),
    constraint contest_riding_club_id_fk
        foreign key (riding_club) references riding_club (id)
);

create or replace table contest_item
(
    id      int auto_increment
        primary key,
    contest int not null,
    item    int not null,
    `rank`  int null,
    constraint contest_item_item_id_fk
        unique (item),
    constraint contest_item_contest_id_fk
        foreign key (contest) references contest (id),
    constraint contest_item_item_id_fk
        foreign key (item) references item (id)
);

create or replace table riding_club_infrastructure
(
    id             int auto_increment
        primary key,
    riding_club    int not null,
    infrastructure int not null UNIQUE,
    constraint riding_club_infrastructure_infrastructure_id_fk
        foreign key (infrastructure) references infrastructure (id),
    constraint riding_club_infrastructure_riding_club_id_fk
        foreign key (riding_club) references riding_club (id)
);

create or replace table riding_club_player
(
    id          int auto_increment
        primary key,
    riding_club int not null,
    player      int not null,
    constraint riding_club_player_player_id_fk
        foreign key (player) references player (id),
    constraint riding_club_player_riding_club_id_fk
        foreign key (riding_club) references riding_club (id)
);

create or replace table transaction
(
    id           int auto_increment
        primary key,
    bank_account int                                   null,
    message      varchar(127)                          not null,
    created_at   timestamp default current_timestamp() not null,
    sum          int                                   not null

) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

create or replace table wound
(
    id   int auto_increment
        primary key,
    name varchar(63) not null
);

create or replace table horse_wound
(
    id    int auto_increment
        primary key,
    horse int null,
    wound int null,
    constraint horse_wound_horse_id_fk
        foreign key (horse) references horse (id),
    constraint horse_wound_wound_id_fk
        foreign key (wound) references wound (id)
);
