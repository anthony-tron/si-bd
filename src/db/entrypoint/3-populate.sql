USE si;

BEGIN;

INSERT INTO player (username, email, password, first_name, last_name, gender, birthday, phone, address, description, web_site, registered_at, lastly_connected_at, media, ip, role)
    VALUES ('unicorn', 'uni@cor.n', '$2y$13$OuLZLiytO/LaY3SN.EjQQ.CAjDpYOjwyd89/oSmy2yTl6kzlvr42.', 'uni', 'corn', 'NON_BINARY', '1970-01-01', '0123456789', null, 'Hello! I''m the best unicorn on the planet.', null, '2022-04-03 20:17:50', null, null, 0x3137322E32322E302E31, 'ROLE_ADMIN');

INSERT INTO player (username, email, password, first_name, last_name, gender, birthday, phone, address, description, web_site, registered_at, lastly_connected_at, media, ip, role)
    VALUES ('publisher', 'publi@sher.n', '$2y$10$p4kWAB5F5ebVNF8SXf79reW1lcDXHjb.k/X48.tlxu9jOO.XZzQCq', 'publi', 'sher', 'NON_BINARY', '1970-01-01', '0123456789', null, 'Hello! I''m the best unicorn on the planet.', null, '2022-04-03 20:17:50', null, null, 0x3137322E32322E302E31, 'ROLE_PUBLISHER');

INSERT INTO player (username, email, password, first_name, last_name, gender, birthday, phone, address, description, web_site, registered_at, lastly_connected_at, media, ip, role)
    VALUES ('admin_horse', 'admin@horse.n', '$2y$10$FItENSeLFyQ/GTwHZCG.quUD.3deBuGUjTigyYguTm0CWLcU1Ck76', 'admin', 'horse', 'NON_BINARY', '1970-01-01', '0123456789', null, 'Hello! I''m the best unicorn on the planet.', null, '2022-04-03 20:17:50', null, null, 0x3137322E32322E302E31, 'ROLE_HORSE_SPECIALIST');

INSERT INTO player (username, email, password, first_name, last_name, gender, birthday, phone, address, description, web_site, registered_at, lastly_connected_at, media, ip, role)
    VALUES ('cron_admin', 'cron@admin.n', '$2y$10$xk2iHObECj38hQimqrIyyuAbLy/POtHO9S7JTpcHpKl9nhcon2euO', 'cron', 'admin', 'NON_BINARY', '1970-01-01', '0123456789', null, 'Hello! I''m the best unicorn on the planet.', null, '2022-04-03 20:17:50', null, null, 0x3137322E32322E302E31, 'ROLE_CRON');

INSERT INTO player (username, email, password, first_name, last_name, gender, birthday, phone, address, description, web_site, registered_at, lastly_connected_at, media, ip, role)
    VALUES ('contest_admin', 'contest@admin.n', '$2y$10$c15di8WsRRzHNuS.y9kkmOtojEEBTwoTl5fTFrTwHapZNViH6CFaG', 'contest', 'admin', 'NON_BINARY', '1970-01-01', '0123456789', null, 'Hello! I''m the best unicorn on the planet.', null, '2022-04-03 20:17:50', null, null, 0x3137322E32322E302E31, 'ROLE_CONTEST_SPECIALIST');

INSERT INTO player (username, email, password, first_name, last_name, gender, birthday, phone, address, description, web_site, registered_at, lastly_connected_at, media, ip, role)
    VALUES ('dev_app', 'dev@app.n', '$2y$10$YMczeZVZvsCD6lS/auJcTOP6oj0T4imO7nC.lYI/Mx5XTD5128K/.', 'dev', 'app', 'NON_BINARY', '1970-01-01', '0123456789', null, 'Hello! I''m the best unicorn on the planet.', null, '2022-04-03 20:17:50', null, null, 0x3137322E32322E302E31, 'ROLE_DEV');

INSERT INTO player (username, email, password, first_name, last_name, gender, birthday, phone, address, description, web_site, registered_at, lastly_connected_at, media, ip, role)
    VALUES ('perform_admin', 'perform@admin.n', '$2y$10$MR603XB4wIYTwjL/X8zKrexUGOXJLW2bWH82YYVwNdqWZLHpnNAU6', 'perform', 'admin', 'NON_BINARY', '1970-01-01', '0123456789', null, 'Hello! I''m the best unicorn on the planet.', null, '2022-04-03 20:17:50', null, null, 0x3137322E32322E302E31, 'ROLE_PERFORMANCE');

INSERT INTO player (username, email, password, first_name, last_name, gender, birthday, phone, address, description, web_site, registered_at, lastly_connected_at, media, ip, role)
    VALUES ('grant_admin', 'grant@admin.n', '$2y$10$RZnaDbcVL48WaMJXsW7lBO1Ih1dxwGcBGierus5hef/jVASt7m1e2', 'perform', 'admin', 'NON_BINARY', '1970-01-01', '0123456789', null, 'Hello! I''m the best unicorn on the planet.', null, '2022-04-03 20:17:50', null, null, 0x3137322E32322E302E31, 'ROLE_GRANT');

COMMIT;
