USE si;


REVOKE All
    ON *
    FROM `si_admin`;

REVOKE GRANT OPTION
    ON *.*
    FROM `grant_admin`;

REVOKE INDEX
    ON *.*
    FROM `performance_admin`;

REVOKE RELOAD, SHUTDOWN, PROCESS
    ON *.*
    FROM `cron_user`;

REVOKE SELECT, INSERT, UPDATE, DELETE
    ON si.*
    FROM `dev`;

REVOKE SELECT, UPDATE
    ON player
    FROM `community_manager`;

REVOKE SELECT, INSERT
    ON media
    FROM `community_manager`;

REVOKE SELECT, UPDATE
    ON horse
    FROM `horse_specialist`;

REVOKE SELECT, UPDATE
    ON horse_disease
    FROM `horse_specialist`;

REVOKE SELECT, UPDATE
    ON horse_item
    FROM `horse_specialist`;

REVOKE SELECT, UPDATE
    ON horse_parasite
    FROM `horse_specialist`;

REVOKE SELECT, UPDATE
    ON horse_wound
    FROM `horse_specialist`;

REVOKE SELECT, INSERT, UPDATE, DELETE
    ON contest
    FROM `contest_specialist`;

REVOKE SELECT, INSERT, UPDATE, DELETE
    ON contest_item
    FROM `contest_specialist`;
REVOKE SELECT, INSERT, UPDATE, DELETE
    ON circuit
    FROM `contest_specialist`;

REVOKE SELECT, INSERT, UPDATE, DELETE
    ON article
    FROM `publisher`;

REVOKE SELECT, INSERT, UPDATE, DELETE
    ON newspaper
    FROM `publisher`;

REVOKE SELECT, INSERT, UPDATE, DELETE
    ON newspaper_article
    FROM `publisher`;

REVOKE SELECT
    ON contest
    FROM `client`;

REVOKE SELECT
    ON contest_item
    FROM `client`;

REVOKE SELECT
    ON article
    FROM `client`;

REVOKE SELECT
    ON newspaper
    FROM `client`;

REVOKE SELECT
    ON newspaper_article
    FROM `client`;

REVOKE SELECT
    ON circuit
    FROM `client`;





