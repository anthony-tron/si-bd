#!/bin/bash

suffix=$(basename $(pwd))

echo Syncing master data with slave...

# creating tmp directory
mkdir tmp 2> /dev/null

# dumping db
docker exec -ti "$suffix"-db-1 mysqldump -B si -pfrosties > tmp/dump.sql

# copying dump of db into replica
docker cp tmp/dump.sql "$suffix"-replica-1:/tmp/dump.sql

# importing data into replica
docker exec -ti "$suffix"-replica-1 sh -c "mysql -pfrosties < /tmp/dump.sql"

master_status=$(docker exec "$suffix"-db-1 sh -c 'mysql -pfrosties -e "SHOW MASTER STATUS;"' | tail -n 1)
master_log=$(echo $master_status | awk '{print $1}')
master_pos=$(echo $master_status | awk '{print $2}')

docker exec "$suffix"-replica-1 sh -c "mysql -pfrosties -e \"STOP SLAVE; CHANGE MASTER TO MASTER_HOST='db', MASTER_USER='replication', MASTER_PASSWORD='replication', MASTER_LOG_FILE='$master_log', MASTER_LOG_POS=$master_pos; START SLAVE;\""
