# Get started

Create `src/app/.env` with the following content:

```
APP_ENV=dev
APP_SECRET=RANDOM_ALPHANUMERIC_TOKEN
DATABASE_URL="mysql://root:@db:3306/si?serverVersion=mariadb-10.3.27&charset=utf8"
```

Up the containers: `docker-compose up`.

Install composer dependencies.

1. `cd src/app`
2. `composer install` (this can take a while)

Visit http://localhost:8080.


## Mapping entities for Symfony

Find the `fpm` container's with `docker ps` and run:

```
docker exec -ti <ID> sh
cd /app
php bin/console doctrine:mapping:import "App\Entity" annotation --path=src/Entity
```


## Recreating the database manually

Drop the `si` schema.

Run the migrations in `src/db/entrypoint`:
1. `1-init.sql`
2. `2-grant.sql`

## Maintenance et surveillance

### Différentes commandes :  

Vérifier toutes les tables et réparer les erreurs avec les deux méthodes :  

MySQL :  

Vérifie  

```
CHECK TABLE <nom de notre table> QUICK 
```

Répare  

```
REPAIR TABLE <nom de notre table> QUICK 
```

Sur chacune des tables mySQL de notre BD 

MyISAM : 

Vérifie  

```
myisamchk -d -c -i –s <chemin vers notre table> 
```

```
myisamchk -a -r –e <chemin vers notre table> 
```

Sur chacune des tables myISAM 

Tâche automatisée :  

On crée un fichier cron-bd.sh contenant :  

```
#!/bin/bash 

myisamchk -d -c -i –s 

myisamchk -a -d 

```
 

Ensuite on lance un cron qui tous les jours à 5h du matin va lancer les commandes dans le fichier .sh 

```
00 05 * * * cron-bd.sh 

```
 

## Politique de surveillance :  

Pour surveiller le plus possible nous logons le plus que l’on peut de mysql 

Pour avoir connaissance de ce qui se produit avec mysqld on le démarre avec l’option --log-warnings=9

Si on veut effectuer une sauvegarde de ces logs :  

```
mv hostname.log hostname-old.log 
mysqladmin flush-logs 
cp hostname-old.log to-backup-directory 
rm hostname-old.log 
```

On peut ensuite les consulter à notre guise  .

